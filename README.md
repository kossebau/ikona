# Ikona
<p align="center">
  <img src="https://invent.kde.org/kde/ikona/raw/master/data/org.kde.Ikona.svg" alt="Ikona"/>
  <br>
  An icon preview utility designed for KDE Plasma
</p>

<p align="center">
  <img width="19%" src="https://invent.kde.org/kde/ikona/raw/master/data/home-screenshot.png" />  
  <img width="19%" src="https://invent.kde.org/kde/ikona/raw/master/data/colour-screenshot.png" />  
  <img width="19%" src="https://invent.kde.org/kde/ikona/raw/master/data/colour-screenshot-dark.png" />  
  <img width="19%" src="https://invent.kde.org/kde/ikona/raw/master/data/monochrome-screenshot.png" />  
  <img width="19%" src="https://invent.kde.org/kde/ikona/raw/master/data/colour-palette-screenshot.png" />  
</p>